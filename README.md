# User Stories

* En tant que lecteur, je peux choisir mon sexe
* En tant que lecteur, je peux renseigner mon prénom pour faire partie intégrante de l'histoire
* En tant que lecteur, je peux choisir entre deux proposition pour accéder à la suite de l'histoire
* En tant que lecteur, je peux revenir sur les choix faits précedement grâce au breadcrumb pour modifier mon avenir
* En tant que lecteur, je peux sauvegarder mon histoire
