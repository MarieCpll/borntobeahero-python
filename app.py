from flask import Flask, render_template, redirect, request
from flask_pymongo import pymongo


CONNECTION_STRING = "mongodb+srv://dbUser:BornToBeAHero31@cluster0-8iyee.mongodb.net/test?retryWrites=true&w=majority"
client = pymongo.MongoClient(CONNECTION_STRING)
db = client.get_database('BornToBeAHero')
user_collection = pymongo.collection.Collection(db, 'user')
app = Flask(__name__)

@app.route('/', methods = ['POST','GET'])
def home():
	sexe = request.form.get('sexe')
	name = request.form.get('name')

	db.user.insert_one({"genre": sexe, "name": name})
	if(sexe == "female"):
		return redirect("/femme")
	elif(sexe == "male"):
		return render_template("homme.html", name=name)
	else:
		return render_template("accueil.html")

@app.route('/femme', methods = ['POST','GET'])
def girl():
	return render_template("femme.html")

if __name__ == "__main__":
	app.run(debug=True)